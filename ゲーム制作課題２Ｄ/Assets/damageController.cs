﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class damageController : MonoBehaviour
{
    GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("player");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-0.1f, 0, 0);

        //画面外に出たらオブジェクトを破棄する
        if (transform.position.x < -10.0f)
        {
            Destroy(gameObject);
        }

        //当たり判定
        Vector2 p1 = transform.position;//矢の中心
        Vector2 p2 = player.transform.position;//プレイヤーの中心
        Vector2 dir = p1 - p2;
        float d = dir.magnitude;
        float r1 = 0.5f;//矢の半径
        float r2 = 1.0f;//プレイヤーの半径

        if (d < r1 + r2)
        {
            Destroy(gameObject);
            GameObject gameDirector = GameObject.Find("GameDirector");
            gameDirector.GetComponent<GameDirector>().DecreaseHp();

        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class damageGenerator : MonoBehaviour
{
    public GameObject damagePrefab;
    public float span = 1.0f;
    float delta = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        delta += Time.deltaTime;//時間を計る
        Debug.Log(Time.deltaTime);
        if (delta > span)//時間が１秒を超えたとき
        {
            delta = 0;//時間を０に戻す

            GameObject go = Instantiate(damagePrefab);
            float py = Random.Range(-4, 4);
            go.transform.position = new Vector3(9.58f, py, 0);
        }
    }
}
